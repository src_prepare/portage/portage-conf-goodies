#!/bin/sh


trap 'exit 128' INT
export PATH
set -e


usage() {
    echo "No proper version provided!" 1>&2
    echo "Usage: ${0} MAJOR MINOR PATCH [-t | --tag]" 1>&2
    exit 1
}

case "${1}" in
    "" | -h | --help )
        usage
        ;;
esac

[ -z "${3}" ] && usage

cd "$(dirname "${0}")"/../src/


version="${1}.${2}.${3}"

find . -type f -exec sed -i "s|@Version:.*|@Version: ${version}|" {} +

case "${4}" in
    -t | --tag )
        git tag "${version}" -m "bump to ${version}"
        ;;
esac
