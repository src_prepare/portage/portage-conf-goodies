#!/bin/bash


# @Author: Maciej Barć <xgqt@riseup.net>
# @Description: After a given repository is synchronized, regenerate it's ebuild cache
# @Homepage: https://gitlab.com/src_prepare/portage/portage-conf-goodies
# @License: CC0-1.0
# @Version: 1.0.0


trap 'exit 128' INT
export PATH
set -e


hook="${0#*-}"

name="${1}"
# ${2} unused
path="${3}"

ncpu="$(nproc || echo 2)"

opts=(
    --jobs="${ncpu}"
    --load-average="${ncpu}"
    --repo="${name}"
    --update
)


if [[ -n "${name}" ]] && [[ -d "${path}" ]]; then
    echo ">>> Running hook: ${hook}"
    echo " Regenerating ebuild cache"
    echo " For repository:  ${name}"
    echo " Into directory:  ${path}"

    egencache "${opts[@]}"

    echo "${hook} finished successfully"
fi
