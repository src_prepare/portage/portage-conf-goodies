#!/bin/bash


# @Author: Maciej Barć <xgqt@riseup.net>
# @Description: After all repositories are synchronized, create a snapshot of /etc/portage
# @Homepage: https://gitlab.com/src_prepare/portage/portage-conf-goodies
# @License: CC0-1.0
# @Version: 1.0.0


trap 'exit 128' INT
export PATH
set -e


hook="${0#*-}"
portage="${EPREFIX}/etc/portage"


echo ">>> Running hook: ${hook}"

# If we don't have git just exit success
type git  >/dev/null 2>&1  || exit 0

cd "${portage}"


if [[ ! -d "${portage}"/.git ]] ; then
    echo "Initializing a git repository in ${portage} ..."

    git init

    git config --get user.email  >/dev/null 2>&1  ||
        git config --local user.email "portageadmin@localhost"

    git config --get user.name   >/dev/null 2>&1  ||
        git config --local user.name "Portage Administrator"
fi


git add .

if git diff-index --quiet HEAD -- ; then
    echo "Nothing to commit"
else
    echo "Creating a commit ..."
    git commit -m "automatic snapshot"
fi


echo "${hook} finished successfully"
