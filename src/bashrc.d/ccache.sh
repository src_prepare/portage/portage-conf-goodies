#!/bin/bash


# @Author: Maciej Barć <xgqt@riseup.net>
# @Description: Set a named CCACHE_DIR for each merged package
# @Homepage: https://gitlab.com/src_prepare/portage/portage-conf-goodies
# @License: CC0-1.0
# @Version: 1.0.0


# Based on Michał Górny's post from 23.07.2017
# https://blogs.gentoo.org/mgorny/2017/07/23/optimizing-ccache-using-per-package-caches/


if [[ "${FEATURES}" == *ccache* && "${EBUILD_PHASE_FUNC}" == src_* ]] ; then
    if [[ "${CCACHE_DIR}" == "${EPREFIX}/var/cache/ccache" ]] ; then

        # Set the ccache dir to whatever CCACHE_DIR is
        # + category + name of the package
        CCACHE_DIR="${EPREFIX}/var/cache/ccache/${CATEGORY}/${PN}"
        export CCACHE_DIR

        mkdir -p "${CCACHE_DIR}" || die "Failed to create CCache directory"

        # Copy ccache.conf if it is found in "old" CCACHE_DIR
        if [[ -f "${EPREFIX}/var/cache/ccache/ccache.conf" ]] ; then
            cp "${EPREFIX}/var/cache/ccache/ccache.conf"  \
               "${EPREFIX}/var/cache/ccache/${CATEGORY}/${PN}/ccache.conf"  ||
                die "Failed to copy the CCache configuration file"
        fi
    fi
fi
