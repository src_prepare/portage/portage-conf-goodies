#!/bin/bash


# @Author: Maciej Barć <xgqt@riseup.net>
# @Description: Inform of current ebuild phase
# @Homepage: https://gitlab.com/src_prepare/portage/portage-conf-goodies
# @License: CC0-1.0
# @Version: 1.0.0


if [[ -n "${EBUILD_PHASE}" ]] && [[ ! "${EBUILD_PHASE}" = depend ]] ; then
    echo "[ $(date +%Y-%m-%d\ %H:%M:%S) ] ${EBUILD_PHASE^^} for version ${PV} of ${PN} from ${CATEGORY}"
fi
