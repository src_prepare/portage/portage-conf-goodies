# Portage Configuration Goodies


<p align="center">
    <a href="https://gitlab.com/src_prepare/portage/portage-conf-goodies/">
        <img src="https://gitlab.com/src_prepare/badge/-/raw/master/hosted_on-gitlab-orange.svg">
    </a>
    <a href="https://gentoo.org/">
        <img src="https://gitlab.com/src_prepare/badge/-/raw/master/powered-by-gentoo-linux-tyrian.svg">
    </a>
    <a href="https://app.element.io/#/room/#src_prepare:matrix.org">
        <img src="https://gitlab.com/src_prepare/badge/-/raw/master/chat-matrix-green.svg">
    </a>
    <a href="https://gitlab.com/src_prepare/portage/portage-conf-goodies/commits/master.atom">
        <img src="https://gitlab.com/src_prepare/badge/-/raw/master/feed-atom-orange.svg">
    </a>
</p>


## About

This repository holds miscellaneous scrips that can be
used in your Portage configuration (/etc/portage).


## License

Licenses for individual files may differ, so
be sure to check a file license that it contains.
